# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

with open('requirements.txt') as f:
	install_requires = f.read().strip().split('\n')

# get version from __version__ variable in bee_logo/__init__.py
from bee_logo import __version__ as version

setup(
	name='bee_logo',
	version=version,
	description='Custom Logo of bee',
	author='firsterp.in',
	author_email='support@firsterp.in',
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
