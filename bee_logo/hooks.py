# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from . import __version__ as app_version

app_name = "bee_logo"
app_title = "Bee Logo"
app_publisher = "firsterp.in"
app_description = "Custom Logo of bee"
app_icon = "octicon octicon-file-directory"
app_color = "grey"
app_email = "support@firsterp.in"
app_license = "MIT"
app_logo_url = "/assets/custom_logo/logo/bee-icon.svg"

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
# app_include_css = "/assets/bee_logo/css/bee_logo.css"
# app_include_js = "/assets/bee_logo/js/bee_logo.js"

# include js, css files in header of web template
# web_include_css = "/assets/bee_logo/css/bee_logo.css"
# web_include_js = "/assets/bee_logo/js/bee_logo.js"

# include js in page
# page_js = {"page" : "public/js/file.js"}

# include js in doctype views
# doctype_js = {"doctype" : "public/js/doctype.js"}
# doctype_list_js = {"doctype" : "public/js/doctype_list.js"}
# doctype_tree_js = {"doctype" : "public/js/doctype_tree.js"}
# doctype_calendar_js = {"doctype" : "public/js/doctype_calendar.js"}

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Website user home page (by function)
# get_website_user_home_page = "bee_logo.utils.get_home_page"

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "bee_logo.install.before_install"
# after_install = "bee_logo.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "bee_logo.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# Document Events
# ---------------
# Hook on document methods and events

# doc_events = {
# 	"*": {
# 		"on_update": "method",
# 		"on_cancel": "method",
# 		"on_trash": "method"
#	}
# }

# Scheduled Tasks
# ---------------

# scheduler_events = {
# 	"all": [
# 		"bee_logo.tasks.all"
# 	],
# 	"daily": [
# 		"bee_logo.tasks.daily"
# 	],
# 	"hourly": [
# 		"bee_logo.tasks.hourly"
# 	],
# 	"weekly": [
# 		"bee_logo.tasks.weekly"
# 	]
# 	"monthly": [
# 		"bee_logo.tasks.monthly"
# 	]
# }

# Testing
# -------

# before_tests = "bee_logo.install.before_tests"

# Overriding Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.desk.doctype.event.event.get_events": "bee_logo.event.get_events"
# }
#
# each overriding function accepts a `data` argument;
# generated from the base implementation of the doctype dashboard,
# along with any modifications made in other Frappe apps
# override_doctype_dashboards = {
# 	"Task": "bee_logo.task.get_dashboard_data"
# }

